<?php
/**
 * WordPress Auto Update
 *
 * @package     MZIB
 * @author      mnap
 * @copyright   2019 WordPress Codex
 * @license     GPLv2
 *
 * @wordpress-plugin
 * Plugin Name: WordPress Auto Update
 * Plugin URI:  https://codex.wordpress.org/Configuring_Automatic_Background_Updates
 * Description: Automatic background updates configuration.
 * Version:     1.0.0
 * Author:      mnap
 * Author URI:  https://gitlab.com/mnap
 * License:     GPLv2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: mzib
 * Domain Path: /languages
 */

// Core development updates, known as the "bleeding edge".
// add_filter( 'allow_dev_auto_core_updates', '__return_true' );
// Minor core updates, such as maintenance and security releases.
add_filter( 'allow_minor_auto_core_updates', '__return_true' );
// Major core release updates.
add_filter( 'allow_major_auto_core_updates', '__return_true' );

// Automatic updates for All plugins.
add_filter( 'auto_update_plugin', '__return_true' );
// Auto-updates for specific plugins only.
// function auto_update_specific_plugins ( $update, $item  ) {
// Array of plugin slugs to always auto-update
// }
// $plugins = array (
// 'akismet',
// 'buddypress',
// );
// if ( in_array( $item->slug, $plugins  )  ) {
// return true; // Always update plugins in this array
// } else {
// return $update; // Else, use the normal API response to decide whether to update or not
// }
// }
// add_filter( 'auto_update_plugin', 'auto_update_specific_plugins', 10, 2 );
// Automatic updates for All themes.
add_filter( 'auto_update_theme', '__return_true' );

// Translation file updates.
add_filter( 'auto_update_translation', '__return_true' );

// Update notification emails.
add_filter( 'auto_core_update_send_email', '__return_true' );
