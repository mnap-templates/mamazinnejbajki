<?php
/**
 * Child theme's function.php file.
 *
 * @package MZIB
 * @author mnap
 */

/**
 * Loads Google Fonts.
 */
function mzib_google_fonts() {
	wp_enqueue_style(
		'mzib-google-fonts',
		'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i|Roboto:400,400i,700,700i&amp;subset=latin-ext',
		false,
		wp_get_theme()->get( 'Version' )
	);
}
add_action( 'wp_enqueue_scripts', 'mzib_google_fonts' );

/**
 * Loads child theme's style file.
 */
function mzib_enqueue_styles() {

	if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
		$mzib_min = '';
	} else {
		$mzib_min = '.min';
	}

	$parent_style = 'lavander-style';

	wp_enqueue_style(
		'mzib-style',
		get_stylesheet_directory_uri() . '/style' . $mzib_min . '.css',
		array( $parent_style ),
		wp_get_theme()->get( 'Version' )
	);
}
add_action( 'wp_enqueue_scripts', 'mzib_enqueue_styles' );

/**
 * Setup MZIB child theme's textdomain.
 *
 * Declares textdomain for this child theme. Overrides parent theme's strings.
 * Translations can be filed in the /languages/ directory.
 */
function mzib_theme_setup() {
	// Load custom translation file for the parent theme.
	load_theme_textdomain(
		'lavander',
		get_stylesheet_directory() . '/languages/lavander'
	);
	// Load translation file for the child theme.
	load_child_theme_textdomain(
		'mzib',
		get_stylesheet_directory() . '/languages'
	);
}
add_action( 'after_setup_theme', 'mzib_theme_setup' );

/**
 * Adds fixed recent posts and most popular posts widgets.
 * Fixes date display in case there in case there is more than one post
 * with the same date.
 */
require_once get_stylesheet_directory() . '/includes/widgets/class-mzib-recent-posts-widget.php';
require_once get_stylesheet_directory() . '/includes/widgets/class-mzib-popular-posts-widget.php';
