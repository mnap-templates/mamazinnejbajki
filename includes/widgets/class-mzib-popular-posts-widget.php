<?php
/** MZIB popular post widget.
 * This is fixed Lavander Premium Popular Posts Widget that displays date in
 * every post.
 *
 * @see https://codex.wordpress.org/Template_Tags/the_date 'SPECIAL NOTE'
 * @see https://wordpress.stackexchange.com/questions/166751/post-doesnt-show-date-if-theres-another-post-with-the-same-date/166766#166766
 *
 * @package MZIB
 */

/**
 * Adds MZIB_Popular_Posts_Widget widget.
 */
class MZIB_Popular_Posts_Widget extends WP_Widget {

	/**
	 * Registers widget with WordPress.
	 */
	public function __construct() {
		// Widget settings.
		$widget_options = array(
			'classname'   => 'category_posts',
			'description' => esc_html__(
				'Displays the post image and excerpt from a most frequently visited posts. Fixed Lavander Premium Widget',
				'mzib'
			),
		);
		// Create the widget.
		parent::__construct(
			'mzib_popular_posts_widget',
			esc_html__( 'MZIB - Popular Posts', 'mzib' ),
			$widget_options,
			array()
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		// User-selected settings.
		$title = ! empty( $instance['title'] )
			? $instance['title']
			: __( 'Popular posts', 'mzib' );
		/**
		 * This filter is documented in WP_Widget_Pages.
		 *
		 * @see wp-includes/widgets/class-wp-widget-pages.php
		 */
		$title  = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = $this->mzib_popular_posts_number_range( $instance['number'] );

		// The query that will get specific posts ordered by views count.
		$pc = new WP_Query(
			array(
				'orderby'             => 'wpb_post_views_count',
				'post_type'           => 'post',
				'showposts'           => $number,
				'nopaging'            => 0,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 1,
			)
		);

		// Display the posts title as a link.
		if ( $pc->have_posts() ) :

			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			echo $args['before_widget'];

			if ( $title ) {
			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				echo $args['before_title'] . $title . $args['after_title'];
			}
			?>

			<?php
			while ( $pc->have_posts() ) :
				$pc->the_post();
				?>

			<div class="widgett">
				<?php
				$image     = '';
				$comment_0 = '';
				$comment_1 = '';
				$comment_2 = '';
				if ( has_post_thumbnail() ) {
					$image = wp_get_attachment_image_src(
						get_post_thumbnail_id( get_the_ID() ),
						'lavander-widget',
						false
					);
					$image = $image[0];
				}
				?>
				<div class="imgholder">
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php esc_attr_e( 'Permanent Link to', 'mzib' ); ?> <?php the_title(); ?>">
						<?php
						if ( has_post_thumbnail( get_the_ID() ) ) {
							echo '<img src = ' . esc_url( $image ) . ' alt = "' . esc_attr( get_the_title() ? get_the_title() : get_the_ID() ) . '"  width="285" height="155" > ';}
						?>
					</a>
				</div>
				<div class="wttitle"><h4><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php esc_attr_e( 'Permanent Link to', 'mzib' ); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h4></div>
				<div class="widget-date"><?php echo esc_html( apply_filters( 'mzib_the_date', get_the_date(), get_option( 'date_format' ), '', '' ) ); ?></div>
			</div>

			<?php endwhile; ?>

			<?php
			wp_reset_postdata();  // Restore global post data stomped by the_post().
		endif;

		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo $args['after_widget'];
	}

	/**
	 * Handles updating for the current widget.
	 * Sanitizes widget title and validates posts number.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		// Strip tags (if needed) and validate entered number.
		$instance['title']  = wp_strip_all_tags( $new_instance['title'] );
		$instance['number'] = $this->mzib_popular_posts_number_range(
			$new_instance['number']
		);

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		// Sets up some default widget settings.
		$defaults = array(
			'title'  => 'Popular Posts',
			'number' => 5,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'mzib' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'mzib' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" value="<?php echo esc_attr( $instance['number'] ); ?>" type="number" min="1" max="10" />
			<br /><small><?php esc_attr_e( '(at most 10)', 'mzib' ); ?></small>
		</p>
		<?php
	}

	/**
	 * Sets number to desired range for this widget.
	 * Returns integer value.
	 *
	 * @param int $number Number of posts set by user.
	 */
	private function mzib_popular_posts_number_range( $number ) {
		// Set default, minimum and maximum number of posts for this widget.
		$def = 5;
		$min = 1;
		$max = 10;

		if ( ! $number ) {
			$number = $def;
		} elseif ( $number < $min ) {
			$number = $min;
		} elseif ( $number > $max ) {
			$number = $max;
		}

		return intval( $number );
	}

	/**
	 * Parses string to lowercase with dashes.
	 *
	 * @param string $string Name to convert to slug.
	 */
	public function slug( $string ) {
		$slug = trim( $string );
		$slug = preg_replace( '/[^a-zA-Z0-9 -]/', '', $slug ); // Only take alphanumerical characters, but keep the spaces and dashes too...
		$slug = str_replace( ' ', '-', $slug ); // Replace spaces by dashes.
		$slug = strtolower( $slug ); // Make it lowercase.
		return $slug;
	}
}

/** Add function to widgets_init that'll load our widget. */
add_action( 'widgets_init', 'mzib_register_popular_posts_widget' );
/** Function that registers our widget. */
function mzib_register_popular_posts_widget() {
	register_widget( 'MZIB_Popular_Posts_Widget' );
}
?>
