# Mama z innej bajki

Private repository for theme building. This theme is used on https://mamazinnejbajki.pl blog.

This is child theme for Lavander Lite, a free version of Lavander by PremiumCoding theme.

## Initial setup for child theme | man-hours

* working environment | 2
* WordPress, theme, plugins installation | 1
* staging website setup, FTP file transfer | 3
* customization, fixes | 8
* images and icons | 2

## Changelog

### 1.0.1

* Fixed translations.

### 1.1.0

* Added automatic updates mu-plugin.

### 1.2.0

* Significant code refactoring:
	- fixed widget class: constructor, number validation, code linting;
	- fixed single post page: linting;
	- adjust project configuration;
	- adjust functions and source files.
* Minimized CSS version loaded in production environment.
* Fixed translations.
* Relicense to GPLv2 to match WordPress license.

### 1.2.1

* Fix license link.
* Move changelog to README.
* Update README.

